Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root(to: 'home#index')
  get('home' => 'home#index')
  get('about' => 'about#index')
  get('consoles' => 'consoles#index')

  namespace :api do
    get('status' => 'status#index')
    get('consoles' => 'consoles#index')
    get('consoles_data_structs' => 'consoles#data_structs')
  end
end
