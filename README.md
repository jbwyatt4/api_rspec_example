# Introduction

A simple RSpec with both JSON API and HTML spec and feature tests.

## What was done

Used the following tutorial for initial RSpec walkthrough.

https://www.youtube.com/watch?v=Wb3oIfiLdZU

## Tech Notes

app created with

rails new .. --api -T

-T leaves out the testing framework

add to dev and test group in gemfile

```ruby
group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'rspec-rails' # add me
end
```

install rspec

rails generate rspec:install
