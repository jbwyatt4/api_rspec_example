class Api::ConsolesController < ApplicationController
  def index

    if params[:manufacturer].present?
      consoles = Console.where(manufacturer: params[:manufacturer])
    else
      consoles = Console.all
    end

    render(json: { 'consoles' => consoles.map(&:formatted_name) })
  end

  def data_structs
    consoles = [
      { name: 'NES', manufacturer: 'Nintendo' },
      { name: 'SNES', manufacturer: 'Nintendo' },
      { name: 'Wii', manufacturer: 'Nintendo' },
      { name: 'Genesis', manufacturer: 'Sega' },
      { name: 'XBox', manufacturer: 'Microsoft' },
      { name: 'Switch', manufacturer: 'Nintendo' },
      { name: 'PS1', manufacturer: 'Sony' },
      { name: 'PS2', manufacturer: 'Sony' }
    ]

    if params[:manufacturer].present?
      consoles = consoles.select do |c|
        c[:manufacturer] == params[:manufacturer]
      end
    end

    render(json: { 'consoles' => consoles.map {
        |c| c[:name]
    }})
  end
end
