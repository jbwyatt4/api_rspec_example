require 'rails_helper'

RSpec.describe 'Status requests' do
  describe 'GET /api/status' do
    it 'returns an ok status message' do
      get('/api/status')
      # { status : ok } different than http 200
      expect(response_json['status']).to eql('ok')
    end
    it 'returns a 200 code' do
      get('/api/status')
      expect(response.status).to eql(200)
    end
  end
end
