require 'rails_helper'

RSpec.describe 'Console requests' do

  before do
    Console.create( name: 'NES', manufacturer: 'Nintendo' )
    Console.create( name: 'SNES', manufacturer: 'Nintendo' )
    Console.create( name: 'Wii', manufacturer: 'Nintendo' )
    Console.create( name: 'Genesis', manufacturer: 'Sega' )
    Console.create( name: 'XBox', manufacturer: 'Microsoft' )
    Console.create( name: 'Switch', manufacturer: 'Nintendo' )
    Console.create( name: 'PS1', manufacturer: 'Sony' )
    Console.create( name: 'PS2', manufacturer: 'Sony' )
  end

  describe 'GET /api/consoles' do
    it 'returns an array of video games consoles' do
      get('/api/consoles')
      expect(response_json['consoles']).to contain_exactly(
        'Nintendo NES',
        'Nintendo SNES',
        'Nintendo Wii',
        'Sega Genesis',
        'Microsoft XBox',
        'Nintendo Switch',
        'Sony PS1',
        'Sony PS2'
      )
    end
    it 'returns an array of some consoles' do
      get('/api/consoles')
      expect(response_json['consoles']).to include(
        'Nintendo NES',
        'Nintendo SNES',
        'Nintendo Wii',
        'Sega Genesis',
        'Microsoft XBox',
        'Nintendo Switch',
        'Sony PS1',
        'Sony PS2'
      )
    end
  end
  describe 'GET /api/consoles_data_structs' do
    it 'returns an array of video games consoles' do
      get('/api/consoles_data_structs')
      expect(response_json['consoles']).to contain_exactly(
        'NES',
        'SNES',
        'Wii',
        'Genesis',
        'XBox',
        'Switch',
        'PS1',
        'PS2'
      )
    end
    it 'supports specifying consoles for a specific manufacturer' do
      get('/api/consoles_data_structs', params: { manufacturer: 'Nintendo'})
      expect(response_json['consoles']).to contain_exactly(
        'NES',
        'SNES',
        'Wii',
        'Switch'
      )
    end
  end
end
